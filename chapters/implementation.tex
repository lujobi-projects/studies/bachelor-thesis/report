\chapter{Implementation}\label{ch:implementation}
In order to conduct a benchmark, a protocol has to be defined, describing the setup, soft- and hardware definitions as well as the procedure. To automate said procedure the benchmark CLI tool has been created in a second step. 

\section{Protocol/Procedure}\label{ch:protocol}

This section explains how the benchmark has to be conducted and especially \textit{why} specific decisions for the protocol were made.
Note that the complete benchmark protocol as it is used in conjunction with the benchmark-CLI can be found in the appendix \ref{ap:comp_prot} or in the Duckietown developer manual \cite{dt_manual}. A visualization is visible in figure \ref{fig:bm_prot_vis}.
\begin{figure}[!htb]
    \centering
    \includegraphics[width=.6\textwidth]{img/protocol/protocol.png}
    \caption{Benchmark Protocol}
    \label{fig:bm_prot_vis}
\end{figure}
\newpage
\subsection{Environment}
Until now all benchmarks have been conducted on 3x3 loop (4 straight tiles and 4 curves). However,  the benchmark - in its current setup - doesn't depend on a specific map and can be used with any map compliant with the Duckietown specifications. As the camera is rather sensible to light from the front it is important to ensure that \textbf{artificial light sources from the ceiling are the only lights around}. Too much light is not good either as the yellow dashed middle line is perceived as white. Additionally,  it is important to place the tiles on a dark (preferably black) ground in order to not disturb the lane detection.

\subsection{Hardware definition}
A list of all used versions and production rounds can be found in appendix \ref{ap:sec:hw_def}. Five main categories (\verb+DB17+, \verb+DB18+, \verb+DB19+, \verb+DB20+, \verb+DB21/DBv2+) are defined. The \verb+DB18+ is the one currently distributed in different labs around the world. That's why there were four production rounds indicated with the appendix \verb+p1+, \verb+p2+, \verb+p3+, \verb+p4+. The \verb+DB21/DBv2+ is the new generation using Ackermann steering instead of differential steering. Most relevant differences for the hardware benchmark are the used version of the Raspberry Pi (\verb+3B+, \verb-3B+- and \verb+4+), the version of the PiHut (\verb+v1.0+, \verb+v1.1+, \verb+v2.0+, \verb+v2.1+), the motors (Dagu yellow and Dagu blue with encoder) and the battery (\verb+Bat_v1+, \verb+Bat_v2+, \verb+other+). As this thesis focuses on the performance of different hardware configurations above mentioned definitions are the most relevant. The rest can be found in appendix \ref{ap:sec:hw_def}.
\subsection{Software definition}
To preserve reproducibility, the used software for a specific hardware benchmark needs to be defined and pinned to a version. In order define a specific version, the (SHA-)digests of the specific Docker images were fixed on Mars 6, 2020. These digests were used as it is simple to install the corresponding Docker image via the \verb+init_sd_card+-procedure. To further simplify the setup, an adjusted Duckietown shell is available for the software releases \verb+master19+ and \verb+daffy+. The complete list of fixed images can be found in Appendix \ref{ap:sec:digests}. Please note that not all listed images are installed during \verb+init_sd_card+.
\subsection{Pre-Benchmark} \label{sec:pre_bm}
Before starting the benchmark, several points ensuring comparability across different benchmarks have to be considered. Said points contain collection of meta data, measuring SD-Card speed, syncing time as well as mount points. For the concrete implementation refer to chapter \ref{ch:bm_cli}.
\paragraph{Setup}
The Duckiebot has to be assembled and calibrated according to the Duckiebot manual \cite{dt_manual}. If a benchmark for a specific production round of the bot is being conducted, it is essential to \textbf{not mix} any parts and only use the ones provided in the box. Apart from the hardware setup, the Duckietown shell has to be installed on the local machine which provides a tool to conduct the benchmark.
\paragraph{SD-Card}
In order to test the SD card read and write speed the test provided in the elinux-Raspberry Pi wiki \cite{wiki:rpispeed} was used. Executing the \verb+dd+ command from and to \verb+/dev/zero+. 
\paragraph{Meta}
Several meta data relevant for the benchmark can't be collected automatically, e.g. benchmark user/location, installed battery, used hardware configuration. This needs to be entered manually. Other meta data like camera version, Raspberry Pi production number, etc. can be retrieved via commands executed on the bot. 
\paragraph{Syncing Time}
As most data is recorded according to the system time of the Duckiebot syncing the time before the benchmark is appropriate. This is is done via a shell command using \verb+ntpd+. 
\paragraph{USB stick}
Once the above steps have been completed, a USB stick used to record any bags to needs to be mounted. The command \verb+sudo mount ...+ is used in order to do so. ROS bags are recorded to the USB stick in order to not disturb the setup on the Raspberry Pi, respectively not use more space than needed on the SD-Card. Especially since the \verb+DB18p1+ is shipped with a 16 GB SD-Card.
\subsection{Benchmark Procedure} \label{sec:bm_procedure}
The lane following demo is used as base to benchmark the Duckiebot. As it has the advantage that it is easy to set up, well documented/defined, a lot of troubleshooting information is available and it works with any size of Duckietown map. 
\\
Most of the hardware measurements are retrieved via the Duckietown diagnostics tool \cite{dt-diagnostics}, lane following information is retrieved directly from the Duckiebot. Metadata not entered by the user is read directly form the diagnostics or the pre benchmark procedure.
Once the Duckiebot has been set up, the metadata has been collected and the robot was placed on the lane, the following steps are applied:
\begin{enumerate} %Todo add more lane descriptions perhaps the commands
    \item \textbf{Start diagnostics}, in case it is not installed on the Duckiebot yet, it will be done by the Duckietown shell. Run for 150s.
    \item \textbf{Start lane following}, as the CPU usage spikes during the first ~5s, due to the diagnostics starting, let it settle for the first 30s after. Then start the lane following demo. 
    \item \textbf{Start bag recording}, 90s after starting the diagnostics a ROS bag is recorded on the Duckiebot (onto the USB stick) collecting lane following data like lane detector latency and detected lane segments. This is done for another 90s.
    \item \textbf{Stop lane following} once the recording of the ROS bag is over.
\end{enumerate}
Above mentioned steps describe the manual procedure, this has been implemented in the benchmark CLI found in section \ref{ch:bm_cli}.
\subsection{Score Calculation} \label{ssec:score_calc}
From the diagnostics and the metadata a benchmark score has to be calculated. This is done by calculating a weighted average from the percentage deviation from a the \textbf{weighted average} of its optimal value (see \ref{sssec:weigh_avg}) for each metric. For the exact list including the specific weights, refer to appendix \ref{ap:bm_score_weights}. The score is calculated from the deviation of the following measurements: 
\begin{itemize}
    \item \textbf{Lane following}, lane detector latency, detected lane segments, CPU and RAM usage
    \item \textbf{Engineering}, Overall RAM, SWAP and CPU usage SD-card read and write speed
    \item \textbf{Container}, RAM and CPU usage of all separate containers
    \item \textbf{Health}, consisting of Raspberry Pi status and throttling status, CPU temperature.
    \item \textbf{Total}, average of the above scores 
\end{itemize}
\subsubsection{Weighted average per benchmark metric}\label{sssec:weigh_avg}
Calculating the weighted average can be done in three ways, "{}punishing"{} upper or lower outliers (i.e. letting them have a larger weight) or the arithmetic mean. Having the possibility to punish outliers which deviate in a specific direction is important: E.g. taking a set of 200 data points of the Raspberry Pi status measurement and all running in \verb+warn+ ($= 1$) mode as the battery is on low voltage. One single outlier of said metric towards error ($= 2$) (Throttling!!!) should potentially be punished more than one outlier towards \verb+ok+ ($= 0$). This is currently done using the following weighing function (assuming $x\in[0, 100]$):
\begin{align}
    f_{upper}(x) &= e^{a \cdot x^b}, \text{where } a = 7.91 \cdot 10^{-4}, b = 1.732  \label{eq:punish_upper} \\ 
    f_{lower}(x) &= f_{upper}(100-x) \label{eq:punish_lower}
\end{align}
The upper most outliners have a ten times larger weight in this equation than the lowest \eqref{eq:punish_upper}, minimal weight is one, and the middle ($x = 50$) has a weight of $2$. The equation \eqref{eq:punish_lower} is the same function mirrored at $y=50$ and as such weighing the lower outliers more. Both functions are visible in image \ref{fig:weighing_fct}; blue displaying equation \eqref{eq:punish_upper}  and green displaying \eqref{eq:punish_lower}.

\begin{figure}[!htb]
    \centering
    \begin{tikzpicture}
        \begin{axis}[
            domain=0.125:105,
            xmin=-5, xmax=105,
            ymin=-1, ymax=11,
            samples=400,
            axis y line=center,
            axis x line=middle,
            height=7cm,
            width=\textwidth,
            legend style={
            at={(0.5,0.75)},
            anchor=north},
        ]
            \addplot [
                    domain=0:101,
                    color=blue,
                ] {e^(0.000791*x^(1.732))};
            \addplot [
                    domain=0:101,
                    color=green,
                ] {e^(0.000791*(100-x)^(1.732))};
            \addplot [
                    domain=0:100, 
                    samples=100, 
                    color=red,
                ]
                {10};
            \addplot [
                    domain=0:50, 
                    samples=100, 
                    color=red,
                ]
                {2};
            \addplot [color=red] coordinates {(50, 0) (50, 2)};
            \addplot [color=red] coordinates {(100, 0) (100, 10)};
            \addlegendentry{$f_{upper}(x)$};
            \addlegendentry{$f_{lower}(x)$};
        \end{axis}
    \end{tikzpicture}
    \caption{Weighing functions}
    \label{fig:weighing_fct}
\end{figure}

\subsubsection{Current score calculation}\label{sssc:curr_score_calc}
In its current settings the score is calculated according to the following pattern: 
\begin{align}
    score &= min \left(0, max \left(100,\left(\frac{100}{max\_value}\cdot\left(max\_value-weighted\_average\right)\right)\right) - 0.5 \cdot c_v\right)
\end{align}

Where $max\_value$ denotes a maximally reachable value, defined as a constant in the config of the benchmark. $weighted\_average$ is calculated as explained in \ref{sssec:weigh_avg}, and $c_v$ is the coefficient of variation (see \ref{eq:coeff_var}). 

\subsubsection{Adjusting the score weights/calculation}
Emphasizing different measurements with different weight currently in use is crucial in order to guarantee before mentioned flexibility. All weights displayed in appendix \ref{ap:bm_score_weights} and all values and categories from \ref{ssec:score_calc} are editable via a configuration file. Adding more data from different sources to the benchmark calculation can be done in the same manner.

\clearpage
\subsection{Score interpretation}
As visible in paragraph \ref{sssc:curr_score_calc}, the score depends highly on the defined $max\_value$. The optimal values are currently selected rather conservative meaning that a score of 100 is barely reachable. For example of the health score, a score of 100 is only possible if the CPU temperature is not raised at all and doesn't fluctuate. \textit{Thus, an overall score in the realm of 60 can be assumed to be good.}

\vspace{5mm}
Some notes on the higher level influence of different inputs on the scores. The only score \textit{directly} affected by the environment (e.g. illumination)  is the lane following score. The health score could technically be improved by a lower room temperature.  Since we are talking about a rather intertwined system for example the influence of bad illumination ans thus, a high segments count has an influence on the CPU usage in a second step. This is due to the surplus of segments to be analyzed. Same applies to all other measurements and score categories. Battery related issues are first visible in the health score. Computaional power related issues are represented in Engineering and Container.

\clearpage
\section{Benchmark CLI Tool}\label{ch:bm_cli}
In order to facilitate and most importantly unify the benchmarking procedure, at least partial automation was indispensable. The higher the automation the better consistency can be guaranteed. That's why the benchmarking CLI tool has been created which is included into the Duckietown shell commands \cite{dt-shell-cmd}. 
As displayed in figure \ref{fig:bm_big_three}, the complete tool consists of three separate services:

\begin{enumerate}
    \item \textbf{Benchmark API/Backend}, analysis and storage of the data
    \item \textbf{Benchmark CLI-Tool}, execution of the benchmark, collection and upload of the data 
    \item \textbf{Benchmark Frontend}, display of the achieved score, manual upload
\end{enumerate}

Where ever reasonable the services were set up as Docker images, respectively included in already existing tools to simplify the setup. In order to facilitate adjusting parameters, the addition of new features and benchmark procedures/protocols or later changes, the tools are kept modular. \\
The software is splitted up into three parts in order to facilitate deploying, especially if one wants to have one central database storage and to include it into the Duckietown Dashboard \cite{dt-dashboard}. 
\begin{figure}[hbt!]
    \centering
    \includegraphics[width=.5\textwidth]{img/bm_tool/big-three.png}
    \caption{Overview}
    \label{fig:bm_big_three}
\end{figure}


\subsection{Benchmark API}\label{sec:api}
The API is the backbone for the automated testing procedure, respective the evaluation of the collected data. As such the following must-requirements were defined:
\begin{itemize}
    \item \textbf{Provide endpoints}, to upload data and retrieve benchmark scores
    \item \textbf{Run locally and as web-API}
    \item \textbf{Process data}, analyze supplied data, extract benchmark metrics, calculate statistic measurements, prepare for storage
    \item \textbf{Calculate score}, calculate benchmark score from saved benchmarks and an overall score for several selected scores 
    \item \textbf{Store data}, manage storage of data
    \item \textbf{Plot measurements}, similar to the diagnostic tool \cite{dt-diagnostics}
\end{itemize}

Data relevant for calculating the benchmark score are saved either on the Duckiebot or diagnostic tool \cite{dt-diagnostics}. Therefore,  the API needs to collect data from different sources as well as save them to different destinations. In order provide above mentioned endpoints the decision was made to create a REST API.

The complete communication between the different benchmark services and external existing (and work in progress) Duckietown services are found in figure \ref{fig:bm-network}.

\begin{figure}[hbt!]
    \centering
    \includegraphics[width=.9\textwidth]{img/bm_tool/auto-bm-network.png}
    \caption{Communication}
    \label{fig:bm-network}
\end{figure}

\subsubsection{Data storage}
The API saves all measured and processed data as a JSON file as well as a summary containing different metadata as well as the averages and weighted averages of all measurements. This is done in order to optimize the calculation of the overall average and the display of multiple benchmark scores.

In order to provide different operation modes (local and online), the API provides different data storage options:
\begin{itemize}
    \item Save benchmark files to a local directory, respective to a local SQLite database 
    \item Save benchmark files to a S3 Bucket (Amazon AWS), respective to a MySQL database
\end{itemize}
Once the Duckietown data API is running in production, saving data directly to S3 can be replaced with said API.

\subsubsection{Endpoints} 
All hardware benchmark related endpoints can be found under \verb+api_url/hw_benchmark+:
\begin{itemize}
    \item The \verb+meta+ endpoint provides all needed benchmark meta which needs to be entered by hand retrieved by a \verb+GET+ request.
    \item The \verb+score+ endpoint provides the average of all submitted benchmarks retrieved by a \verb+GET+ request.
    \item The \verb+files+ endpoint fulfills the following functions:
    \begin{itemize}
        \item \verb+POST+-request to \verb+/hw_benchmark/files+: upload all benchmark files (including the diagnostics)
        \item \verb+GET+-request to \verb+/hw_benchmark/files+: returns a list of 25 benchmarks (pagination is provided) including a small summary
        \item \verb+POST+-request to \verb+/hw_benchmark/files/{key}+: upload all benchmark files, diagnostics are retrieved from the diagnostics tool via the submitted \verb+key+
        \item \verb+GET+-request to \verb+/hw_benchmark/files/{key}+: retrieve all benchmark data as json, \verb+key+ is the uuid of a benchmark
        \item \verb+POST+-request to \verb+/hw_benchmark/files/{key}.png+: retrieve graphs from all measured data as png, \verb+key+ is the uuid of a benchmark
    \end{itemize}
\end{itemize}
\subsubsection{Plots} 
The displayed plots are similar to the plots of the Duckietown diagnostics \cite{dt-diagnostics} and visible in figure \ref{fig:plots}

\begin{figure}[hbt!]
    \centering
    \includegraphics[trim={50 3470 50 4540},clip,width=.65\textwidth ]{img/bm_tool/complete_plot.png}
    \caption{API: Plots}
    \label{fig:plots}
\end{figure}


\subsubsection{Documentation}
A short documentation of each endpoint can be retrieved from the Swagger-UI found under the URL on which the API runs. Additionally,  a web-based option to try out the requests is provided which helps to develop future tools. A small illustration is displayed in figure \ref{fig:swagger_ui}. For a fully functional documentation, have a look at the running API. 

\begin{figure}[hbt!]
    \centering
    \includegraphics[width=.95\textwidth, trim={50 25 35 30}, clip]{img/bm_tool/swagger_ui.png}
    \caption{Swagger UI}
    \label{fig:swagger_ui}
\end{figure}

\newpage
\subsubsection{Implementation}
The API is written in Python using the following libraries:
\begin{itemize}
    \item \textbf{Flask Restplus}, Flask server and Documentation of endpoints, swagger-UI
    \item \textbf{Flask SQLAlchemy}, Handle SQL commands to local and online databases
    \item \textbf{Boto3}, Save data to Amazon S3
    \item \textbf{pyrosbag}, Analyze ROS bags
    \item \textbf{numpy, scipy, matplotlib}, Create Plots, analyze data, calculate score
\end{itemize}

\newpage
\subsection{Benchmark CLI-Tool}
The API processes the data which needs to be retrieved in an exact manner adhering to the specific protocol found in section \ref{ch:protocol} ore more specific in sections \ref{sec:pre_bm} and \ref{sec:bm_procedure}. In order to ensure execution of several different commands at specific times at least some automation on the host computer is needed. This is done via a new command added to the Duckietown shell.

\subsubsection{Assumptions}
The following assumptions have been made in order for the CLI to run successfully:
\begin{itemize}
    \item The Duckiebot is completely and correctly assembled, set up and calibrated
    \item The Raspberry Pi is running, connected to the internet and reachable from the host PC via SSH
    \item The PiHut is complete and correctly flashed
    \item The API is reachable from the Duckiebot
\end{itemize}
Most of those assumptions are verifiable via code but have not been implemented yet as the scope has size of a small project.

\subsubsection{Protocol}
The following list contains the parts of the protocol \ref{ch:protocol} relevant for the benchmark:
\begin{enumerate}
    \item \textbf{Collect metadata}, collecting metadata which can not be retrieved automatically, e.g. benchmark user, benchmark location, battery 
    \item \textbf{Execute Pre-Benchmark}, collecting metadata of the camera and the Raspberry Pi and SD-metrics, syncing time on the Duckiebot
    \item \textbf{Start diagnostics} in order to measure the hardware performance metrics.
    \item \textbf{Start lane-following}, as the diagnostics' CPU usage spikes in the first seconds, start the lane following 30 seconds after starting the diagnostics.
    \item \textbf{Start bag recording}, in order to measure the latency of the lane detector node as well as the amount of detected lane segments.
    \item \textbf{Stop lane-following}, once enough data is collected
    \item \textbf{Submit data to API} in order to process the data and to calculate the benchmark
\end{enumerate}

\subsubsection{Implementation} 
\paragraph{Collecting metadata}
Metadata is either collected via direct user input or by copying a Python file onto the Duckiebot via SCP and then starting it via SSH. This file executes several shell commands, stopping potential conflicting docker containers, collecting information of the Raspberry Pi (e.g. serial number and hardware information), the camera (e.g. camera revision and different settings), measuring SD-card read and write speeds and in the end starting the before stopped containers. All of this data is saved in two JSON files and stored on the Duckiebot.

\paragraph{Start diagnostics, lane following and bag recording}
In a first step the Docker container \verb+duckietown/dt-system-monitor+ is started on the Duckiebot. This might have to be installed first. Once the container is running (and collecting measurements) for 30 seconds lane following should be started. 60 seconds later recording Rosbags has to be started. This is done by applying a regex onto every line of the output of the diagnostics container as it outputs a time stamp. If the regex is fulfilled, the respective callback function is executed, which then executes the next command. As the benchmark needs to be supervised by a user, a message is displayed when to start the lane-following. Recording Rosbags is started from a callback function by executing a shell command on the Duckiebot via SSH. In order to keep measurements consistent the Rosbag is recorded to an USB stick which is mounted first.

\paragraph{Submitting data to the API}
As all the data is stored on the Duckiebot it needs to be transmitted to the API. This is done via CURL. One example can be found below: 
\begin{lstlisting}[style=BashInputStyle, breaklines=true]
curl -X POST "http://127.0.0.1:5000/hw_benchmark/files/v1__test__new__autobot14__1589916105" -F "meta={\"bot_type\":\"DB18p4\",\"battery_type\":\"Old Alu\",\"release\":\"master19\"}" -H  "accept: application/json" -H  "Content-Type: multipart/form-data" -F "sd_card_json=@sd_speed.json" -F "latencies_bag=@meas_01/master19_autobot14_01.bag"  -F "meta_json=@meta.json"
\end{lstlisting}
This command is assembled by the the CLI and then executed on the Duckiebot via SSH.

\paragraph{Inclusion into the Duckietown shell}
The CLI is included into the \verb+dts+ (Duckietown shell). It uses the following additional libraries, which have to be installed by hand.

\begin{itemize}
    \item \textbf{paramiko}, SSH protocol used via Python
    \item \textbf{scp}, copy files via the SCP protocol
    \item \textbf{asyncio, nest\_asyncio}, Run asynchronous processes in Python
    \item \textbf{ptyprocess}, analyze shell commands on the fly
\end{itemize}

\newpage
\subsection{Benchmark Frontend}
The Frontend will be subject to change in order to be incorporated into the Duckietown dashboard\cite{dt-dashboard}. Thus, it is kept rather rudimentary. Nonetheless it provides the full functionality needed to upload benchmarks, display scores and compare them against the overall average. 

\begin{figure}[hbt!]
    \begin{subfigure}{.5\linewidth}
        \centering
        \includegraphics[width=.88\textwidth]{img/bm_tool/overall_ui.png}
        \caption{Overall Score Display}
        \label{fig:frontend_overall}
    \end{subfigure}%
    \begin{subfigure}{.5\linewidth}
        \centering
        \includegraphics[width=.95\textwidth]{img/bm_tool/upload_ui.png}
        \caption{Upload}
        \label{fig:upload_frontend}
    \end{subfigure}\\[1ex]
    \begin{subfigure}{\linewidth}
        \centering
        \includegraphics[width=.95\textwidth]{img/bm_tool/score_ui.png}
        \caption{Score Display}
        \label{fig:frontend_score}
    \end{subfigure}
    \caption{Frontend}
    \label{fig:frontend}
\end{figure}
An illustration can be found in figure \ref{fig:frontend_overall}.
As seen in this figure  an overall benchmark score is calculated which represents the average over all submitted data. In figure \ref{fig:upload_frontend} the manual upload section is visible and in figure \ref{fig:frontend_score} the score of all separate uploads is displayed.

\subsubsection{Implementation}
The Frontend is implemented using \verb+NodeJS, React, Material UI+ and a template by \verb+devias.io+ \cite{devias-io}. In order to easily add other benchmarks all used elements are built modular which enables simple reuse in new displays.