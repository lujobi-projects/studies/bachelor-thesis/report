\chapter{Preliminaries}\label{ch:preliminaries}
%todo
\section{Hardware}
\subsection{Duckiebot}
The Duckiebot is the vehicle of Duckietown which drives autonomously through the city. There are different versions of the Duckiebot (see appendix \ref{ap:sec:hw_def}). All the setup instructions can be found on the official Duckietown documentation \cite{dt_manual}. All current (and the next two) versions of the Duckiebot are differentially driven, and consist of a Raspberry Pi, a custom made PiHut, two motors, a battery and front and rear LEDs. This version is displayed in figure \ref{fig:bot_v1}. The current bots use all the same type of battery, a new one is in production and occasionally used which is able to measure battery statistics.
A new version of the Duckiebot - the \verb+DBv2+ - will be introduced in the future. It uses Ackermann steering and more sensors (IR, IMU, lane following, etc.) which then can be flexibly attached and removed. A prototype is displayed in \ref{fig:bot_v2}.

\begin{figure}[hbt!]
    \begin{subfigure}{.5\linewidth}
        \centering
        \includegraphics[width=.91\textwidth]{img/Raspberry/DBv1.jpg}
        \caption{Duckiebot DB18}
        \label{fig:bot_v1}
    \end{subfigure}%
    \begin{subfigure}{.5\linewidth}
        \centering
        \includegraphics[width=.9\textwidth]{img/Raspberry/DBv2.jpg}
        \caption{Duckiebot DBv2, prototype}
        \label{fig:bot_v2}
    \end{subfigure}
    \caption{Duckietown bot major versions}
    \label{fig:bots}
\end{figure}

\subsubsection{Raspberry Pi}
Raspberry Pis are small, cheap, but fully capable Linux-based computers. It is used as the brain of the Duckiebot and thus, responsible for the communication between different hosts, the peripherals and running Docker and ROS (see \ref{sec:Software}). It is running HypriotOS, a Raspian-based (Debian) operating system optimized for Docker. The three different types of Raspberry Pis are used (or will be used) in Duckietown are displayed in figure \ref{fig:raspis}.

\begin{figure}[hbt!]
    \begin{subfigure}{.33\linewidth}
        \centering
        \includegraphics[width=.85\textwidth]{img/Raspberry/rpi_3b.jpg}
        \caption{Raspberry Pi 3B}
        \label{fig:raspi3}
    \end{subfigure}%
    \begin{subfigure}{.33\linewidth}
        \centering
        \includegraphics[width=.8\textwidth]{img/Raspberry/rpi_3b+.jpg}
        \caption{Raspberry Pi 3B+}
        \label{fig:raspi3p}
    \end{subfigure}
    \begin{subfigure}{.33\linewidth}
        \centering
        \includegraphics[width=.83\textwidth]{img/Raspberry/rpi_4b.jpg}
        \caption{Raspberry Pi 4B}
        \label{fig:raspi4}
    \end{subfigure}
    \caption{Raspberry Pis (source: \url{elinux.org})}
    \label{fig:raspis}
\end{figure}

The \verb+Raspberry Pi 3B+ and \verb'3B+' have 1GB of RAM each while the \verb+Raspberry Pi 4B+, that will be used in future models, has 2GB. The 4B version offers the highest clock frequency (followed by the \verb'3B+') plus it has two USB3.0 Ports and is powered via USB Type C instead of USB2.0 and Micro USB. All Raspberry Pis are equipped with a 4 core CPU using a 64 Bit architecture.
\subsubsection{Pi Huts}
The Pi Huts are used to control and power the motors as well as control the LEDs. It is directly attached to the GPIO pins of the Raspberry Pi. There are 3 different types of Pi Huts used in Duckietown:
\begin{figure}[hbt!]
    \begin{subfigure}{.33\linewidth}
        \centering
        \includegraphics[width=.8\textwidth]{img/Raspberry/hut_v10.png}
        \cprotect\caption{\verb+v1.0+}
        \label{fig:hut_v10}
    \end{subfigure}%
    \begin{subfigure}{.33\linewidth}
        \centering
        \includegraphics[width=.8\textwidth]{img/Raspberry/hut_v20.png}
        \cprotect\caption{\verb+v2.0+}
        \label{fig:hut_v20}
    \end{subfigure}
    \begin{subfigure}{.33\linewidth}
        \centering
        \includegraphics[width=.8\textwidth]{img/Raspberry/hut_v21.png}
        \cprotect\caption{\verb+v2.1+}
        \label{fig:hut_v21}
    \end{subfigure}
    \caption{Raspberry Pi Huts}
    \label{fig:huts}
\end{figure}

Compared with the \verb+v1.0+, the \verb+v1.1+ removed the auto charging feature, as it is redundant with the new battery. The \verb+Hut v2.0+ provides the possibility to install new sensors or LEDs simply as plug and play.

\subsubsection{Batteries}
In the following, two versions of the Duckietown battery are defined, displayed in \ref{fig:bats}. The names \verb+Bat\_v1+ and \verb+Bat\_v2+ will be used for the respective versions.
\begin{figure}[hbt!]
    \begin{subfigure}{.5\linewidth}
        \centering
        \includegraphics[trim={20 100 20 60},clip, width=.58\textwidth]{img/Raspberry/bat_v1.jpg}
        \cprotect\caption{\verb+Bat\_v1+}
        \label{fig:bat_v1}
    \end{subfigure}%
    \begin{subfigure}{.5\linewidth}
        \centering
        \includegraphics[trim={20 140 15 100},clip, width=.7\textwidth]{img/Raspberry/bat_v2.jpg}
        \cprotect\caption{\verb+Bat\_v2+}
        \label{fig:bat_v2}
    \end{subfigure}
    \caption{Duckietown batteries (not to scale)}
    \label{fig:bats}
\end{figure}

Both batteries are first and foremost a powerbank with a output of 2.4 A and a Capacity of 37 Wh. However,  the \verb+Bat\_v2+ contains a micro controller which sends battery metrics. These metrics consist of cell temperature, cycle count, output voltage and amperage, the capacity, etc. 

\newpage
\subsection{Duckietown}
\paragraph{Map}
The Duckietown map defines the setup of the tiles. Generally there are straight, curved and intersection tiles which have to be assembled according to the specifications in \cite{dt_manual}. For the benchmark only the straight and the curved tiles are required, see figure \ref{fig:tile}.
\begin{figure}[hbt!]
    \begin{subfigure}{.5\linewidth}
        \centering
        \includegraphics[width=.5\textwidth]{img/map/straight.png}
        \vspace{6mm}
        \caption{Straight}
        \label{fig:tile_straight}
    \end{subfigure}
    \begin{subfigure}{.5\linewidth}
        \centering
        \includegraphics[width=.55\textwidth]{img/map/curve.png}
        \caption{Curve}
        \label{fig:tile_curve}
    \end{subfigure}
    \caption{Duckietown tiles}
    \label{fig:tile}
\end{figure}

In the following pages the 3x3 loop is often mentioned as it is the simplest map adhering to the Duckietown specifications. It is displayed in figure \ref{fig:loop}.

\begin{figure}[hbt!]
    \centering
    \includegraphics[width=.5\textwidth]{img/map/loop.png}
    \caption{3x3 Loop}
    \label{fig:loop}
\end{figure}

\paragraph{Watchtowers}
Watchtowers are part of the localization system of Duckietown. Each watchtower is equipped with a Raspberry Pi 4 and a camera. It records April tags attached to the Duckiebot and the road tiles. From a recorded image stream and a map, the position and direction of all Duckiebots are calculated after the experiment has been run.
\newpage
\section{Acronyms and Abbreviations}
\begin{tabbing}
 \hspace*{1.6cm}  \= \kill
 ETH \> Eidgen\"{o}ssische Technische Hochschule \\[0.5ex]
 IDSC \> Institute for Dynamic Systems and Control \\[2ex]
 
 API \> Application Program Interface \\[0.5ex]
 CLI \> Command Line Interface \\[0.5ex]
 CPU \> Central Processing Unit \\[0.5ex]
 CURL \> Client for URL, tool for sending request via different protocols from the CLI\\[0.5ex]
 GPIO \> General Purpose Input/Output, Pins on the Raspberry Pi \\[0.5ex]
 HTTP \> Hypertext Transfer Protocol, stateless web communication protocol\\[0.5ex]
 IMU \> Inertial measurement unit\\[0.5ex]
 IR \> Infra red\\[0.5ex]
 RAM \> Random Access Storage \\[0.5ex]
 regex \> Regular Expression, search pattern defined by characters, efficient for find and/or replacement operations\\[0.5ex]
 REST \> Representational State Transfer, a definition of constraints for a web service\\[0.5ex]
 SHA \> Secure Hash Algorithm \\[0.5ex]
 SCP \> Secure Copy Protocol, copy files via SSH from and to a remote host\\[0.5ex]
 SSH \> Secure Shell, access the shell of another host via the network\\[0.5ex]
 UI \> User Interface\\[0.5ex]
 
\end{tabbing}
\newpage
\section{Software}\label{sec:Software}
\subsection{ROS}
The Robot Operating System (ROS) is the central piece of software running on the Duckiebot. It is node-structured, thus bringing flexibility and modularity. Each node implements only a small part of the complete functionality of the robot. E.g the \verb+camera_node+ takes the raw camera image and publishes it on a specific topic. In order to communicate between nodes ROS uses topics to which nodes can subscribe (read messages) and publish (send messages). In principle it is a message bus, categorized by the topics. A message is always attached to one topic. Said topic defines the content of a specific message. The source code of each node is compiled separately, thus a single node can be exchanged or adjusted without touching the rest of the code. At startup all nodes need to be registered to a ROS-Master which manages all the nodes. For this thesis the most important functionality of ROS is, that it enables recording messages from different topics which can later be played back or analyzed.
\subsection{RESTful API}
A RESTful API is a interface used to store, modify, retrieve and delete data via the \verb+http+ protocol. REST defines constraints for webservices how to communicate and is as such a standard for communication between webservices e.g. front- and backend. It is subdivided into small modules which each fulfill a sub function of the overall API. Being stateless, no information exchange can happen between different calls. REST APIs may support the following methods: \verb+GET+, \verb+DELETE+, \verb+POST+, \verb+PUT+, \verb+PATCH+.

\section{Math}
Only minimal knowledge of statistics is needed:

\paragraph{Mean/Average}
The average is defined as in equation \eqref{eq:mean}. The terms mean, average and arithmetic mean are used equivalently.
\begin{align} 
    \bar{x}=\mu=\frac{1}{n}\sum_{i=1}^n x_i\label{eq:mean}
\end{align}
\paragraph{Weighted average}
The weighted average is a generalization of the arithmetic mean and allows to weigh different measures differently. The general definition is found in equation \eqref{eq:weighed_avg}. In the benchmark the weights are function of $x_i$, weighing specific outliers more.
\begin{align} 
    \bar{x} &= \frac{ \sum\limits_{i=1}^n w_i x_i}{\sum\limits_{i=1}^n w_i}\label{eq:weighed_avg},
    w_i = f(x_i)
\end{align}
\paragraph{Median}
"{}A median is a value separating the higher half from the lower half of a data sample"{}\cite{wiki:median}. As such for a \textit{}{ordered} dataset of $n$ measurements it is defined as in equation \eqref{eq:median} where $\lfloor\cdot\rfloor$ denotes floor division and $\lceil\cdot\rceil$ ceil division.
\begin{align}
    \mathrm{median}(x) = \frac{1}{2} (x_{\lfloor (n+1)/2\rfloor} + x_{\lceil (n+1)/2\rceil}) \label{eq:median}
\end{align}
Note: As visible in figure \ref{fig:median} this isn't necessarily the same number as the average. 
\begin{figure}[!htb]
    \begin{subfigure}{.5\linewidth}
        \centering
        \begin{tikzpicture}
            \begin{axis}[
                domain=0.125:10,
                xmin=-1, xmax=11,
                ymin=-1, ymax=11,
                samples=400,
                axis y line=center,
                axis x line=middle,
                height=7cm,
                width=\textwidth,
                legend style={
                at={(0.9,0.9)},
                anchor=north},
            ]
                \addplot [
                        domain=0:101,
                        color=green,
                    ] {5};
                \addplot [
                        domain=0:101,
                        color=blue,
                    ] {4};
                \addplot+[
                    color=red,
                    only marks,
                    mark=*,]
                coordinates{(1, 3)(2, 5)(3, 8)(4, 10)(5, 3)(6, 8)(7, 8)(8, 3)(9, 0)(10, 2)};
                \addlegendentry{$mean$};
                \addlegendentry{$median$};
            \end{axis}
        \end{tikzpicture}
        \caption{Points scattered}
    \end{subfigure}
    \begin{subfigure}{.5\linewidth}
        \centering
        \begin{tikzpicture}
            \begin{axis}[
                domain=0.125:10,
                xmin=-1, xmax=11,
                ymin=-1, ymax=11,
                samples=400,
                axis y line=center,
                axis x line=middle,
                height=7cm,
                width=\textwidth,
                legend style={
                at={(1,0.25)},
                anchor=east},
            ]
                \addplot [
                        domain=0:101,
                        color=green,
                    ] {5};
                \addplot [
                        domain=0:101,
                        color=blue,
                    ] {4};
                \addplot+[
                    color=red,
                    only marks,
                    mark=*,]
                coordinates{(3, 3)(6, 5)(8, 8)(10, 10)(4, 3)(9, 8)(7, 8)(5, 3)(1, 0)(2, 2)};
                \addplot[
                        color=blue,
                        dashed
                    ]
                coordinates{(5.5,0)(5.5,10)};
                \addlegendentry{$mean$};
                \addlegendentry{$median$};
            \end{axis}
        \end{tikzpicture}
        \caption{ordered data points}
    \end{subfigure}
    \caption{Mean vs. median (using 10 data points)}
    \label{fig:median}
\end{figure}


\paragraph{Standard deviation}
"{}The standard deviation is a measure of the amount of variation or dispersion of a set of values. A low standard deviation indicates that the values tend to be close to the mean (also called the expected value) of the set, while a high standard deviation indicates that the values are spread out over a wider range."{} \cite{wiki:std} This helps us determine the "{}stability"{} of a measurement.
\begin{align}
    \sigma = \sqrt{\frac{1}{n} \sum_{i=1}^n (x_i - \mu)^2},\text{ where } \mu = \frac{1}{n} \sum_{i=1}^n x_i.\label{eq:std}
\end{align}
\paragraph{Coefficient of variation}
The calculation of the benchmark score uses a kind of standard deviation which is not attached to any units. The coefficient of variation or relative standard deviation \cite{wiki:coeff_var} gives a measure of the standard deviation relative to the corresponding mean. This is defined in equation \eqref{eq:coeff_var}.
\begin{align}
    c_{\rm v} = \frac{\sigma}{\mu}\label{eq:coeff_var}
\end{align}